import os

from stilt_stability_checks.config.base import *  # noqa

if os.environ.get('ENVIRONMENT') == 'production':
    from stilt_stability_checks.config.prod import *  # noqa
else:
    from stilt_stability_checks.config.stage import *  # noqa

from stilt_stability_checks.config.local import *  # noqa
