"""CLI entry for running checks"""
from argparse import ArgumentParser

from colorama import Fore
from colorama import Style
from internal.mysql.apis import SQLConnector

from stilt_stability_checks.checks.base import CheckPeriod
from stilt_stability_checks.checks.base import CheckResult
from stilt_stability_checks import config
from stilt_stability_checks.checks import CHECKS


def run_checks(
        connector: SQLConnector,
        period: CheckPeriod = None,
        name=None,
        send_error_email=False
):
    passed = 0
    failed = 0
    errored = 0
    for check in CHECKS:
        print(f'{Style.DIM}Running check "{check.name}"{Style.RESET_ALL}')
        if (period == CheckPeriod.ANY or check.period == period and (
                not name or name == check.name)):
            result = check(connector, send_error_email=send_error_email)

            print('', end='\033[F')  # go to prev line
            print(f'{check.name}'.ljust(80, '_'), end='')
            if result == CheckResult.PASSED:
                print(f'{Fore.GREEN}PASSED{Fore.RESET}')
                passed += 1
            elif result == CheckResult.ERROR:
                print(f'{Fore.RED}{Style.BRIGHT}ERROR{Style.RESET_ALL}')
                errored += 1
            else:
                print(f'{Fore.RED}FAILED{Fore.RESET}')
                failed += 1
    print(
        f'{Fore.GREEN}PASSED: {passed}{Fore.RESET} '
        f'{Fore.RED if failed else ""}FAILED: {failed} '
        f'{Fore.RED if errored else ""}ERRORS: {errored}'
    )


def set_config(conf_file):
    conf = __import__(conf_file)
    for k, v in conf.__dict__.items():
        locals()[k] = v


if __name__ == '__main__':
    parser = ArgumentParser(description='Stability checks runner.')
    parser.add_argument(
        'period', type=CheckPeriod, help='start date',
    )
    parser.add_argument(
        '--conf', type=str, help='conf file', required=False
    )
    parser.add_argument(
        '--send_email', type=bool, help='Send error email on fail',
        required=False,
    )

    args = parser.parse_args()
    conf_file = args.conf
    period = args.period or CheckPeriod.ANY

    if conf_file:
        set_config(conf_file)

    connector = SQLConnector(
        rdb_user=config.RDS_DB_USER,
        rds_host=config.RDS_SERVER_HOST,
        rdb_password=config.RDS_DB_PASSWORD,
        db_name=config.RDS_DB_NAME,
    )

    run_checks(connector, period)
