from datetime import date
from datetime import timedelta

from stilt_stability_checks.checks.base import CheckFailedException
from stilt_stability_checks.checks.base import CheckPeriod
from stilt_stability_checks.checks.base import check


@check(period=CheckPeriod.DAY)
def shadow_logs_daily_check(connector):
    """Ensure shadow logs were created yesterday"""
    today_date = date.today() - timedelta(days=1)
    today_logs_count = connector.run_select(
        f"""select count(*) as count from loan_shadow_logs
        where added > {today_date}
        """
    )[0]['count']

    if today_logs_count == 0:
        raise CheckFailedException(
            message=f'No shadow logs were create since {today_date}.'
        )


@check(period=CheckPeriod.WEEK)
def shadow_logs_weekly_check(connector):
    """Ensure there there is no huge gap in shadow_logs"""
    from_date = date.today() - timedelta(days=7)
    loan_active_status = '98a1db23a6da50f78843df0b2abc71c1'
    loans_count = connector.run_select(
        f"""select count(*) as count from loans
        where added > {from_date}
        and status_id = '{loan_active_status}'
        """
    )[0]['count']

    logs_count = connector.run_select(
        f"""select count(*) as count from loan_shadow_logs
        where added > {from_date}
        """
    )[0]['count']

    if logs_count < loans_count:
        raise CheckFailedException(
            message=f'Too few shadow logs are created since {from_date}.'
        )
