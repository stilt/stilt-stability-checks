import time

from stilt_stability_checks.checks.base import CheckPeriod
from stilt_stability_checks.checks.base import check


@check(period=CheckPeriod.ANY)
def test_check(connector):
    """Dummy check for testing"""
    time.sleep(1)
