"""
Check registry.
Add here your check class to make it enabled.
"""
from .base import *  # noqa
from .shadow_logs import shadow_logs_daily_check
from .shadow_logs import shadow_logs_weekly_check
from .test_check import test_check

CHECKS = [
    test_check,
    shadow_logs_daily_check,
    shadow_logs_weekly_check
]
