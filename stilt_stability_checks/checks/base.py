"""Health Check structure API

To create a new health check add a new file <your_check>.py under checks dir or
add new func to some existing file:

@check(period=CheckPeriod.ANY)
def my_check(connector):
    ...

"""
import logging
import time
from datetime import datetime
from enum import Enum

from internal.mysql.apis import SQLConnector
from stilt_communication.wrappers.email.client import StiltMailClient

LOGGER = logging.getLogger(__name__)
RESULT_TABLE = 'health_check_results'


class CheckPeriod(Enum):
    HOUR = 'hour'
    DAY = 'day'
    WEEK = 'week'
    ANY = 'any'


class CheckResult(Enum):
    ERROR = 'error'
    FAILED = 'failed'
    PASSED = 'passed'


class CheckFailedException(Exception):
    def __init__(self, message, data=None):
        self.data = data
        self.message = message

    def __str__(self):
        return f'{self.message} {self.data or ""}'


def save_result_to_db(
        connector: SQLConnector,
        check_name: str,
        *,
        start_time: float,  # timestamp from time.time()
        end_time: float,  # timestamp from time.time()
        result: CheckResult,
        details: str = None,
):
    """Save check result to database"""

    start_time = datetime.fromtimestamp(start_time).strftime(
        '%Y-%m-%d %H:%M:%S')
    end_time = datetime.fromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S')
    connector.insert(
        RESULT_TABLE, {
            'name': check_name,
            'start_time': start_time,
            'end_time': end_time,
            'result': result.value,
            'details': details,
        }
    )


def check(period):
    def wrapper(f):
        def wrapped(
                connector: SQLConnector,
                send_error_email=False,
                save_to_db=False
        ):
            mail_client = StiltMailClient(connector)
            logging.info(f'Starting health check "{f.__doc__}"')
            start_time = time.time()
            try:
                f(connector)

                end_time = time.time()
                logging.info(
                    f'Health check "{f.__doc__}" passed in '
                    f'{end_time - start_time: {.3}f} sec'
                )
                if save_to_db:
                    save_result_to_db(
                        connector,
                        f.__name__,
                        result=CheckResult.PASSED,
                        start_time=start_time,
                        end_time=end_time,
                    )
                return CheckResult.PASSED
            except CheckFailedException as e:
                end_time = time.time()
                msg = (f'Health check "{f.__doc__}" failed in '
                       f'{end_time - start_time: {.3}f} sec. Message: {e}')
                logging.warning(msg)
                if send_error_email:
                    mail_client.send_error_email(
                        message=msg,
                        subject=f'Health check "{f.__doc__}" failed'
                    )

                if save_to_db:
                    save_result_to_db(
                        connector,
                        f.__name__,
                        result=CheckResult.FAILED,
                        start_time=start_time,
                        end_time=end_time,
                    )

                return CheckResult.FAILED
            except Exception as e:
                end_time = time.time()
                logging.error(
                    f'Error running health check "{f.__doc__}": {e}'
                )
                if send_error_email:
                    mail_client.send_error_email(
                        message=f'Error: {e}',
                        subject=f'Error running health check "{f.__doc__}"'
                    )

                if save_to_db:
                    save_result_to_db(
                        connector,
                        f.__name__,
                        result=CheckResult.ERROR,
                        start_time=start_time,
                        end_time=end_time,
                        details=str(e),
                    )

                return CheckResult.ERROR

        wrapped.name = f.__doc__ or f.__name__
        wrapped.period = period
        return wrapped

    return wrapper
