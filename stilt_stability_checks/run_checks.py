"""Module for running all health checks"""
from internal.mysql.apis import SQLConnector

from stilt_stability_checks.checks.base import CheckPeriod
from stilt_stability_checks.checks import CHECKS


def run_checks(connector: SQLConnector, period: CheckPeriod = CheckPeriod.ANY,
               send_error_email=False):
    results = {}
    for check in CHECKS:
        if period == CheckPeriod.ANY or check.period == period:
            results[check.name] = check(
                connector, send_error_email=send_error_email
            )

    return results
