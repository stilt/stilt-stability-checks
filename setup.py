from setuptools import find_packages
from setuptools import setup

setup(
    name='stilt-stability-checks',
    version='0.0.1-2',
    author='Vitali Levitski',
    author_email='v.levytskyi-nerdysoft@stilt.com',
    description='Stilt stability checks tasks',
    url='',
    py_modules=['stilt_stability_checks'],
    packages=find_packages(),
    install_requires=[
        'stilt-mysql>=1.3.0',
        'colorama>=0.4.4'
    ],
    python_requires='>=3.7',
)
