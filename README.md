# Stilt heath checks 

Package for different health checks, mostly data checks.
It can be run as a cron service or locally as script.

The main aim is to automate running different sql queries or other data 
retirement to ensure that some service is doing well.

### Creating a new check:
To create a new health check add a new file <your_check>.py under a */checks* dir.
Create a class with HeathCheck as a base class.
Set name and period class variables.
    
    from stilt_stability_checks.checks import check, CheckPeriod

    @check(period=CheckPeriod.ANY)
    def my_check(connector):
        result = sql_connector.run_query("...")
        if some_thing_wrong:
            raise CheckFailedException("Describe what is wrong here")
        else:
            # just do nothing, no exception - check passed!
            pass


### To Run as server:
    cd stilt_stability_checks
    ENV=<stage | prod> flask run

    curl http://localhost:8000/run_checks/<week|day|hour|any>/

### To run as script
    python run_checks.py week|day|hour|any
or:

    python -m stilt_stability_checks week|day|hour|any
    >>
    Shadow logs check____________________________PASSED
    Audit logs check_____________________________FAILED

### Development:
- Ensure it's working locally when making changes:
  > python -m stilt-stability-checks any
- Please keep flake8 style :)
  > flake8 stilt_stability_checks

### Deployment:
Run following command to deploy function to staging:

    run_checks_daily 
    --runtime=python37 
    --trigger-http 
    --region=us-central1 
    --egress-settings=private-ranges-only 
    --entry-point=run_checks_daily 
    --ingress-settings=all 
    --memory=128MB  
    --timeout=30s 
    --max-instances=2 
    --vpc-connector=default-vpc-connector 
    --set-env-vars=SYSTEM_VERSION_COMPAT=1,ENVIRONMENT=staging 
    --project=stilt-staging

(to deploy to prod, change --project and ENVIRONMENT=production)

### Cron trigger:
After the function was deployed, create a cloud scheduler cron with http 
trigger, take url from deployment command output, or from gcloud console.