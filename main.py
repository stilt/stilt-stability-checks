"""
Google functions entry point.

Add new function here when new cron task is needed:
    def run_check_<new_period>:
        ...
"""
from internal.mysql.apis import SQLConnector

from stilt_stability_checks.run_checks import run_checks
from stilt_stability_checks.checks.base import CheckPeriod
from stilt_stability_checks.config import *

connector = SQLConnector(
    rdb_user=RDS_DB_USER,
    rds_host=RDS_SERVER_HOST,
    rdb_password=RDS_DB_PASSWORD,
    db_name=RDS_DB_NAME,
)


def run_checks_daily(*_, **__):
    run_checks(connector, CheckPeriod.DAY, send_error_email=True)


def run_checks_weekly(*_, **__):
    run_checks(connector, CheckPeriod.WEEK, send_error_email=True)
